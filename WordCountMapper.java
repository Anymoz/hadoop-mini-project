import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.io.IntWritable;

    
    
    public class WordCountMapper extends Mapper<LongWritable, Text, Text, IntWritable> {

      private final static IntWritable value = new IntWritable(1);
      private Text word = new Text();
        
        public void map(LongWritable key, Text txtValue, Context context) throws IOException, InterruptedException {
          String line = txtValue.toString();
          String[] words = line.split("[^\\p{L}]+");
          
          for (String w : words) {
            if (w.length() > 0) {
              word.set(w.toLowerCase());
              context.write(word, value);
            }
          }
        }
        }
